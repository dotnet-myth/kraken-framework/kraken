﻿using Blazored.LocalStorage;
using Kraken.Domain.Interfaces.Authentication;
using Kraken.Domain.Models.Authentication;
using Microsoft.AspNetCore.Components.Authorization;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Kraken.Application.AccessControl.Authentication {

    public class KrakenAuthenticationStateProvider: AuthenticationStateProvider {

        private readonly ILocalStorageService _localStorageService;

        private readonly IUserManager _userManager;

        public KrakenAuthenticationStateProvider( ILocalStorageService localStorageService, IUserManager userManager ) {
            _localStorageService = localStorageService;
            _userManager = userManager;
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync( ) {
            var user = await _localStorageService.GetItemAsync<AuthUser>( "UserAuthenticated" );

            if ( user != null )
                return await MarkUserAsAuthenticated( user );

            return await MarkUserAsLoggedOutAsync( );
        }

        public Task<AuthenticationState> MarkUserAsAuthenticated( AuthUser user ) {
            _localStorageService.SetItemAsync<AuthUser>( "UserAuthenticated", user );
            _userManager.SetUser( user );

            var authenticatedUser = new ClaimsPrincipal(
                new ClaimsIdentity( new[ ] {
                    new Claim( ClaimTypes.NameIdentifier, user.UserId.ToString() ),
                    new Claim( ClaimTypes.Name, user.Username ),
                    new Claim( ClaimTypes.Email, user.Email ),
                    new Claim( "AcessToken", user.Token ),
                }, "ApiAuth" )
            );

            var authState = Task.FromResult( new AuthenticationState( authenticatedUser ) );
            NotifyAuthenticationStateChanged( authState );

            return authState;
        }

        public Task<AuthenticationState> MarkUserAsLoggedOutAsync( ) {
            _localStorageService.RemoveItemAsync( "UserAuthenticated" );
            _userManager.RemoveUser( );

            var anonymousUser = new ClaimsPrincipal( new ClaimsIdentity( ) );
            var authState = Task.FromResult( new AuthenticationState( anonymousUser ) );
            NotifyAuthenticationStateChanged( authState );

            return authState;
        }
    }
}