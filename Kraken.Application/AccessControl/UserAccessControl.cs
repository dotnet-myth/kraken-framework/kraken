﻿using Kraken.Application.AccessControl.Authentication;
using Kraken.Domain.Interfaces.Authentication;
using Kraken.Domain.Models.Authentication;
using Microsoft.AspNetCore.Components.Authorization;
using System.Threading.Tasks;

namespace Kraken.Application.AccessControl {

    public class UserAccessControl: IUserAccessControl {

        private readonly KrakenAuthenticationStateProvider _authenticationStateProvider;

        public UserAccessControl( AuthenticationStateProvider authenticationStateProvider ) =>
            _authenticationStateProvider = ( authenticationStateProvider as KrakenAuthenticationStateProvider );

        public void SetUserLoggedIn( AuthUser user ) =>
            _authenticationStateProvider.MarkUserAsAuthenticated( user );

        public async Task SetUserLoggedOut( ) =>
            await _authenticationStateProvider.MarkUserAsLoggedOutAsync( );
    }
}