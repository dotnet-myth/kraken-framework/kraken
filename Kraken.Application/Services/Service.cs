﻿using Blazored.LocalStorage;
using Kraken.Domain.Interfaces.Services;
using System.Net.Http;

namespace Kraken.Application.Services {

    public abstract class Service: IService {

        protected HttpClient _client;

        protected ILocalStorageService _localStorageService;

        public Service( HttpClient client, ILocalStorageService localStorageService ) {
            _client = client;
            _localStorageService = localStorageService;
        }

        public Service( IHttpClientFactory clientFactory, string connectionName, ILocalStorageService localStorageService )
            : this( clientFactory.CreateClient( connectionName ),
                  localStorageService ) { }
    }
}