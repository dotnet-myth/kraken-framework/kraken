﻿function goBack() {
    window.history.back();
}

function setTextVariables(textPrimary, textSecondary, textDisabled) {
    var html = document.getElementsByTagName('html')[0];

    html.style.setProperty("--text-primary-color", textPrimary);
    html.style.setProperty("--text-secondary-color", textSecondary);
    html.style.setProperty("--text-disabled-color", textDisabled);
}

function setBackgroundVariables(backgroundPrimary, backgroundSecondary, backgroundDark) {
    var html = document.getElementsByTagName('html')[0];

    html.style.setProperty("--background-primary-color", backgroundPrimary);
    html.style.setProperty("--background-secondary-color", backgroundSecondary);
    html.style.setProperty("--background-dark-color", backgroundDark);
}