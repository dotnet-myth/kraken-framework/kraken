using System.Collections.Generic;

namespace Kraken.Domain.Models.Directory {

    public class DisksResponse {
        public IEnumerable<string> Disks { get; set; }

        public DisksResponse( ) {
            Disks = new List<string>( );
        }

        public DisksResponse( IEnumerable<string> disks ) {
            Disks = disks;
        }
    }
}