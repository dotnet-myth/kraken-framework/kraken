using System.Web;

namespace Kraken.Domain.Models.Directory {

    public class GetDirectoryResponse {
        public string Path { get; set; }

        public bool IncludeHidden { get; set; }

        public GetDirectoryResponse( ) {
        }

        public GetDirectoryResponse( string path, bool includeHidden ) {
            Path = HttpUtility.UrlDecode( path );
            IncludeHidden = includeHidden;
        }
    }
}