namespace Kraken.Domain.Models.Directory {

    public class PostLibraryResponse {
        public string Path { get; set; }

        public long UserId { get; set; }

        public PostLibraryResponse( string path, long userId ) {
            Path = path;
            UserId = userId;
        }
    }
}