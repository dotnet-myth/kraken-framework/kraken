using System.Collections.Generic;

namespace Kraken.Domain.Models.Directory {

    public class DirectoryResponse {
        public DirectoryItemResponse Current { get; set; }

        public DirectoryItemResponse Parent { get; set; }

        public IEnumerable<DirectoryItemResponse> Children { get; set; }

        public DirectoryResponse( ) {
            Current = new DirectoryItemResponse( );
            Parent = new DirectoryItemResponse( );
            Children = new List<DirectoryItemResponse>( );
        }

        public DirectoryResponse( DirectoryItemResponse current, IEnumerable<DirectoryItemResponse> children )
            : this( ) {
            Current = current;
            Children = children;
        }
    }
}