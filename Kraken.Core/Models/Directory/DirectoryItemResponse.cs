namespace Kraken.Domain.Models.Directory {

    public class DirectoryItemResponse {
        public string Name { get; set; }

        public string Path { get; set; }

        public DirectoryItemResponse( ) {
        }

        public DirectoryItemResponse( string name, string path ) {
            Name = name;
            Path = path;
        }
    }
}