namespace Kraken.Domain.Models.Directory {

    public class GetRootDirectoryResponse {
        public bool IncludeHidden { get; set; }

        public GetRootDirectoryResponse( ) {
        }

        public GetRootDirectoryResponse( bool includeHidden ) {
            IncludeHidden = includeHidden;
        }
    }
}