﻿using Kraken.Domain.Interfaces.Authentication;

namespace Kraken.Domain.Models.Authentication {

    public class UserCurrent: IUserCurrent {
        protected AuthUser User { get; set; }

        public long GetId( ) {
            return User.UserId;
        }

        public string GetImage( ) {
            return User?.Image;
        }
    }
}