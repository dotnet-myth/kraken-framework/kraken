﻿using Kraken.Domain.Interfaces.Authentication;

namespace Kraken.Domain.Models.Authentication {

    public class UserManager: UserCurrent, IUserManager {

        public void SetUser( AuthUser user ) =>
            User = user;

        public string GetToken( ) =>
            User.Token;

        public void RemoveUser( ) =>
            User = null;

        public bool IsValid( ) =>
            User != null;
    }
}