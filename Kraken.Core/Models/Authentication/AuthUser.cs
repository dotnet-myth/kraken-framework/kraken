﻿namespace Kraken.Domain.Models.Authentication {

    public class AuthUser {
        public long UserId { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string Token { get; set; }

        public string Image { get; set; }

        public AuthUser( ) {
        }

        public AuthUser( long userId, string username, string email, string token, string image ) {
            UserId = userId;
            Username = username;
            Email = email;
            Token = token;
            Image = image;
        }
    }
}