namespace Kraken.Domain.Models.Authentication.Responses {

    public class ValidateResponse {
        public string Username { get; set; }

        public string Token { get; set; }

        public ValidateResponse( ) {
        }

        public ValidateResponse( string username, string token ) {
            Username = username;
            Token = token;
        }
    }
}