namespace Kraken.Domain.Models.Authentication.Responses {

    public class ValidResponse {
        public bool Valid { get; set; }

        public string Username { get; set; }

        public string Token { get; set; }

        public ValidResponse( ) {
        }

        public ValidResponse( bool valid, string username, string token ) {
            Valid = valid;
            Username = username;
            Token = token;
        }
    }
}