﻿using BlazorPro.Spinkit;
using Kraken.Domain.Interfaces.Options;

namespace Kraken.Domain.Models.Options {

    public class KrakenOptions: IKrakenOptions {
        public static KrakenOptions Default => new KrakenOptions( );

        public int HttpPort { get; set; } = 80;

        public int? HttpsPort { get; set; } = null;

        public SpinnerType SpinnerType { get; set; } = SpinnerType.Circle;
    }
}