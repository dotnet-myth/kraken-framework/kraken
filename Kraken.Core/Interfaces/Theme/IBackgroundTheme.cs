﻿using Kraken.Domain.Interfaces.Theme.Base;

namespace Kraken.Domain.Interfaces.Theme {

    public interface IBackgroundTheme: ITheme {
        string Dark { get; }
    }
}