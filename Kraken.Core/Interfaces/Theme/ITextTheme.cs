﻿using Kraken.Domain.Interfaces.Theme.Base;

namespace Kraken.Domain.Interfaces.Theme {

    public interface ITextTheme: ITheme {
        string Disabled { get; }
    }
}