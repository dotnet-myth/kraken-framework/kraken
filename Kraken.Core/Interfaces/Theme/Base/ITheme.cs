﻿namespace Kraken.Domain.Interfaces.Theme.Base {

    public interface ITheme {
        string Primary { get; }

        string Secondary { get; }
    }
}