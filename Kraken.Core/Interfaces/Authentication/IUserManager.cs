﻿using Kraken.Domain.Models.Authentication;

namespace Kraken.Domain.Interfaces.Authentication {

    public interface IUserManager: IUserCurrent {

        void SetUser( AuthUser user );

        void RemoveUser( );

        string GetToken( );

        bool IsValid( );
    }
}