﻿namespace Kraken.Domain.Interfaces.Authentication {

    public interface IUserCurrent {

        public long GetId( );
    }
}