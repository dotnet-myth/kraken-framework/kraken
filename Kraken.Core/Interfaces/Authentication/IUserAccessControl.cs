﻿using Kraken.Domain.Models.Authentication;
using System.Threading.Tasks;

namespace Kraken.Domain.Interfaces.Authentication {

    public interface IUserAccessControl {

        void SetUserLoggedIn( AuthUser user );

        Task SetUserLoggedOut( );
    }
}