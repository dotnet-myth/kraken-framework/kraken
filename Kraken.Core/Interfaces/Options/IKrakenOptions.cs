﻿using BlazorPro.Spinkit;

namespace Kraken.Domain.Interfaces.Options {

    public interface IKrakenOptions {
        int HttpPort { get; set; }

        int? HttpsPort { get; set; }

        SpinnerType SpinnerType { get; set; }
    }
}