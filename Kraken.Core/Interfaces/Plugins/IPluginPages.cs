﻿using System.Collections.Generic;
using System.Reflection;

namespace Kraken.Domain.Interfaces.Plugins {

    public interface IPluginPages: IEnumerable<Assembly> {
    }
}