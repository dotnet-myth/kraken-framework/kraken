﻿using Microsoft.Extensions.DependencyInjection;

namespace Kraken.Domain.Interfaces.Plugins {

    public interface IPluginService {

        void Register( IServiceCollection services );

        IPluginPages ExportPages( );
    }
}