﻿using AutoMapper;
using Blazored.LocalStorage;
using Blazorise;
using Blazorise.Bootstrap;
using Blazorise.Icons.FontAwesome;
using Kraken.Application.AccessControl;
using Kraken.Application.AccessControl.Authentication;
using Kraken.Domain.Interfaces.Authentication;
using Kraken.Domain.Interfaces.Options;
using Kraken.Domain.Interfaces.Services;
using Kraken.Domain.Interfaces.Theme.Base;
using Kraken.Domain.Models.Authentication;
using Kraken.Domain.Models.Options;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection;

namespace Kraken.IoC {

    public static class InjectorContainer {

        private static IEnumerable<Assembly> _customAssemblies;

        private static IEnumerable<Assembly> _localAssemblies;

        private static IEnumerable<Type> _localTypes;

        public static IEnumerable<Assembly> LocalAssemblies {
            get {
                if ( _localAssemblies == null || !_localAssemblies.Any( ) )
                    _localAssemblies = GetAssemblies( );
                return _localAssemblies;
            }
        }

        public static IEnumerable<Type> LocalTypes {
            get {
                if ( _localTypes == null || !_localTypes.Any( ) )
                    _localTypes = LocalAssemblies.GetTypes( );
                return _localTypes;
            }
        }

        private static IEnumerable<Assembly> GetAssemblies( ) {
            var current = AppDomain.CurrentDomain
                .GetAssemblies( )
                .Where( x => !x.IsDynamic );

            if ( _customAssemblies != null && _customAssemblies.Any( ) )
                current.Concat( _customAssemblies );

            var localFiles = Directory.GetFiles( AppDomain.CurrentDomain.BaseDirectory, "*.dll", SearchOption.TopDirectoryOnly );

            var files = localFiles
                .Except( current.Select( x => x.Location ) );

            var localAssemblies = new List<Assembly>( );

            foreach ( var item in files )
                try {
                    var assembly = Assembly.LoadFrom( item );
                    assembly.GetExportedTypes( );
                    localAssemblies.Add( assembly );
                } catch ( Exception ) { }

            var assemblies = current
                .Concat( localAssemblies )
                .Where( a => !a.IsDynamic )
                .ToList( );

            return assemblies;
        }

        private static IEnumerable<Type> GetTypes( this IEnumerable<Assembly> assemblies ) =>
            assemblies
                .SelectMany( x => x.GetExportedTypes( ) )
                .Where( t => {
                    try {
                        return t.IsClass && !t.IsAbstract && !t.Namespace.Contains( "Kraken" );
                    } catch ( Exception ) { return false; }
                } )
                .ToList( );

        private static IServiceCollection AddRange( this IServiceCollection services, IEnumerable<Type> typesList, string interfaceName = default, ServiceLifetime serviceLifetime = ServiceLifetime.Scoped ) {
            foreach ( var type in typesList ) {
                var @interface = interfaceName;

                if ( string.IsNullOrEmpty( interfaceName ) )
                    @interface = type.Name;

                var interfaceType = type
                     .GetInterfaces( )
                     .FirstOrDefault( i => i.Name.ToLower( ).Contains( @interface.ToLower( ) ) );

                var descriptor = new ServiceDescriptor( interfaceType, type, serviceLifetime );
                services.Replace( descriptor );
            }
            return services;
        }

        public static IServiceCollection AddCustomAssemblies( this IServiceCollection services, params Assembly[ ] assemblies ) {
            if ( _customAssemblies == null )
                _customAssemblies = new List<Assembly>( );
            _customAssemblies = _customAssemblies.Concat( assemblies );
            return services;
        }

        public static IServiceCollection AddKraken( this IServiceCollection services, Action<IKrakenOptions> options = null, params Assembly[ ] assemblies ) {
            var settings = KrakenOptions.Default;
            options.Invoke( settings );

            services.AddCustomAssemblies( assemblies );

            services.AddThirdLibraries( );
            services.AddHttpContextAccessor( );
            services.AddKrakenSettings( settings );
            services.AddThemes( );
            services.AddAccessControl( );
            services.AddServices( );
            services.AddMapper( );
            services.AddMediatR( );

            return services;
        }

        public static IServiceCollection AddThirdLibraries( this IServiceCollection services ) {
            services.AddBlazorise( options => options.ChangeTextOnKeyPress = true );
            services.AddBootstrapProviders( );
            services.AddFontAwesomeIcons( );
            services.AddBlazoredLocalStorage( );

            return services;
        }

        public static IServiceCollection AddKrakenSettings( this IServiceCollection services, IKrakenOptions settings ) {
            services.AddSingleton<IKrakenOptions>( settings );
            return services;
        }

        public static IServiceCollection AddAccessControl( this IServiceCollection services ) {
            var manager = new UserManager( );
            services.AddScoped<AuthenticationStateProvider, KrakenAuthenticationStateProvider>( );
            services.AddSingleton<IUserManager, UserManager>( x => manager );
            services.AddSingleton<IUserCurrent, UserManager>( x => manager );
            services.AddScoped<IUserAccessControl, UserAccessControl>( );
            return services;
        }

        public static IServiceCollection AddHttpClient( this IServiceCollection services, string name, int httpPort = 80, int? httpsPort = null, string urlAppends = "api/", string token = default ) {
            services.AddHttpClient( name, ( serviceProvider, client ) => {
                var httpContext = serviceProvider.GetService<IHttpContextAccessor>( ).HttpContext.Request;
                var https = httpContext.IsHttps;
                var scheme = httpContext.Scheme;
                var port = https ? httpsPort : httpPort;
                var ip = httpContext.Host.Host;

                client.BaseAddress = new Uri( $"{scheme}://{ip}{( port != 80 ? $":{port}" : "" )}/{urlAppends}" );

                if ( token != default )
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue( "Bearer", token );
            } );

            return services;
        }

        public static IServiceCollection AddMediatR( this IServiceCollection services ) {
            services.AddMediatR( x => x.AsScoped( ), LocalAssemblies.ToArray( ) );
            return services;
        }

        public static IServiceCollection AddMapper( this IServiceCollection services ) {
            services.AddAutoMapper( cfg => {
                cfg.Advanced.AllowAdditiveTypeMapCreation = true;
            }, LocalAssemblies );

            services
                .BuildServiceProvider( )
                .GetService<IMapper>( )
                .ConfigurationProvider.AssertConfigurationIsValid( );

            return services;
        }

        public static IServiceCollection AddServices( this IServiceCollection services ) {
            var queries = LocalTypes.Where( t => typeof( IService ).IsAssignableFrom( t ) );
            services.AddRange( queries );
            return services;
        }

        public static IServiceCollection AddThemes( this IServiceCollection services ) {
            var queries = LocalTypes.Where( t => typeof( ITheme ).IsAssignableFrom( t ) );
            services.AddRange( queries );
            return services;
        }

        public static IApplicationBuilder UseKraken( this IApplicationBuilder app ) {
            app
                .ApplicationServices
                .UseBootstrapProviders( )
                .UseFontAwesomeIcons( );

            return app;
        }
    }
}