﻿using Kraken.Domain.Interfaces.Plugins;
using Kraken.Plugins.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Reflection;

namespace Kraken.Plugins.Container {

    public abstract class PluginService: IPluginService {

        public virtual void Register( IServiceCollection services ) {
        }

        public virtual IPluginPages ExportPages( ) {
            var assemblies =
             AppDomain.CurrentDomain.GetAssemblies( )
                .Where( t => !( t.EntryPoint != null && t.EntryPoint.Name == "Main" && t.EntryPoint.ReturnType == typeof( void ) ) )
                .SelectMany( x => x.ExportedTypes )
                .Where( t => t.IsSubclassOf( typeof( ComponentBase ) ) )
                .Where( t => {
                    var attr = t.GetCustomAttributes( ).OfType<RouteAttribute>( );
                    return attr != null && attr.Any( );
                } )
                .Select( x => x.Assembly );

            return new PluginPages( assemblies );
        }
    }
}