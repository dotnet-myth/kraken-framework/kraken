﻿using Kraken.Domain.Interfaces.Plugins;
using System.Collections.Generic;
using System.Reflection;

namespace Kraken.Plugins.Models {

    public class PluginPages: List<Assembly>, IPluginPages {

        public PluginPages( ) : base( ) {
        }

        public PluginPages( IEnumerable<Assembly> pages ) : base( pages ) {
        }
    }
}